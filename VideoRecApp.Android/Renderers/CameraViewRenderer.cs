﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.Hardware.Camera2;
using Android.Hardware.Camera2.Params;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Java.Util;
using Java.Util.Concurrent;
using Javax.Microedition.Khronos.Egl;
using Javax.Microedition.Khronos.Opengles;
using VideoRecApp.CustomRenderer;
using VideoRecApp.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CameraView), typeof(CameraViewRenderer))]
namespace VideoRecApp.Droid.Renderers
{
    public class CameraViewRenderer : ViewRenderer<CameraView, AutoFitTextureView>
    {
        private SparseIntArray ORIENTATIONS = new SparseIntArray();
        private MyCameraStateCallback stateListener;
        private MySurfaceTextureListener surfaceTextureListener;

        public CameraDevice cameraDevice;
        public CameraCaptureSession previewSession;
        public MediaRecorder mediaRecorder;
        public VideoView videoView;

        private Android.Util.Size videoSize;
        private Android.Util.Size previewSize;

        public CaptureRequest.Builder builder;
        private CaptureRequest.Builder previewBuilder;

        private HandlerThread backgroundThread;
        private Handler backgroundHandler;

        public Semaphore cameraOpenCloseLock = new Semaphore(1);

        private bool isRecordingVideo;
        private string _fileName;

        private SurfaceView _watermark;

        public CameraViewRenderer(Context context) : base(context)
        {
            ORIENTATIONS.Append((int)SurfaceOrientation.Rotation0, 90);
            ORIENTATIONS.Append((int)SurfaceOrientation.Rotation90, 0);
            ORIENTATIONS.Append((int)SurfaceOrientation.Rotation180, 270);
            ORIENTATIONS.Append((int)SurfaceOrientation.Rotation270, 180);
            surfaceTextureListener = new MySurfaceTextureListener(this);
            stateListener = new MyCameraStateCallback(this);
            _watermark = new SurfaceView(Context);  
         //   _watermark.Holder.AddCallback(this);
         
        }

        public void OpenCamera(int width, int height)
        {
            if (Context == null/* || Context.IsFinished*/)
                return;

            CameraManager manager = (CameraManager)Context.GetSystemService(Context.CameraService);
            try
            {

                if (!cameraOpenCloseLock.TryAcquire(2500, TimeUnit.Milliseconds))
                    throw new Java.Lang.RuntimeException("Time out waiting to lock camera opening.");
                string cameraId = manager.GetCameraIdList()[1];
                CameraCharacteristics characteristics = manager.GetCameraCharacteristics(cameraId);
                StreamConfigurationMap map = (StreamConfigurationMap)characteristics.Get(CameraCharacteristics.ScalerStreamConfigurationMap);
                videoSize = ChooseVideoSize(map.GetOutputSizes(Class.FromType(typeof(MediaRecorder))));
                previewSize = ChooseOptimalSize(map.GetOutputSizes(Class.FromType(typeof(MediaRecorder))), width, height, videoSize);
                int orientation = (int)Resources.Configuration.Orientation;
                if (orientation == (int)Android.Content.Res.Orientation.Landscape)
                {
                    Control.SetAspectRatio(previewSize.Width, previewSize.Height);
                }
                else
                {
                    Control.SetAspectRatio(previewSize.Height, previewSize.Width);
                }
                ConfigureTransform(width, height);
                mediaRecorder = new MediaRecorder();
                manager.OpenCamera(cameraId, stateListener, null);

            }
            catch (CameraAccessException)
            {
                // Toast.MakeText(Activity, "Cannot access the camera.", ToastLength.Short).Show();
                //Activity.Finish();
            }
            catch (NullPointerException)
            {
                //var dialog = new ErrorDialog();
                //dialog.Show(FragmentManager, "dialog");
            }
            catch (InterruptedException)
            {
                throw new RuntimeException("Interrupted while trying to lock camera opening.");
            }
        }

        public void StartPreview()
        {
            if (null == cameraDevice || !Control.IsAvailable || null == previewSize)
                return;

            try
            {
                //ImageView img = new ImageView(Context);
                //img.SetAdjustViewBounds

                SetUpMediaRecorder();
                SurfaceTexture texture = Control.SurfaceTexture;
                texture.SetDefaultBufferSize(previewSize.Width, previewSize.Height);
                previewBuilder = cameraDevice.CreateCaptureRequest(CameraTemplate.Record);
                var surfaces = new List<Surface>();

                var previewSurface = new Surface(texture);
                surfaces.Add(previewSurface);
                previewBuilder.AddTarget(previewSurface);

                var recorderSurface = mediaRecorder.Surface;



                //int[] attribute_list = {
                //        EGL14.EglContextClientVersion, 3,
                //         EGL14.EglNone };
               // var mEGLDisplay = EGL14.EglGetDisplay(EGL14.EglDefaultDisplay);
               // var mEGLConfig = EGL14.EglGetConfigs(EGL14.EglSlowConfig);
               // Android.Opengl.EGL14.EglCreateWindowSurface(mEGLDisplay, Android.Opengl.EGL14.EglSlowConfig, recorderSurface, attribute_list, 0);
                surfaces.Add(recorderSurface);
                previewBuilder.AddTarget(recorderSurface);

                cameraDevice.CreateCaptureSession(surfaces, new PreviewCaptureStateCallback(this), backgroundHandler);

            }
            catch (CameraAccessException e)
            {
                e.PrintStackTrace();
            }
            catch (IOException e)
            {
                //e.PrintStackTrace();
            }
        }

        private void SetUpMediaRecorder()
        {

            if (Context == null)
                return;

            mediaRecorder.SetAudioSource(AudioSource.Mic);
            mediaRecorder.SetVideoSource(VideoSource.Surface);
            mediaRecorder.SetOutputFormat(OutputFormat.Mpeg4);
            mediaRecorder.SetOutputFile(GetVideoFile().AbsolutePath);
            mediaRecorder.SetVideoEncodingBitRate(10000000);
            mediaRecorder.SetVideoFrameRate(30);
            mediaRecorder.SetVideoSize(videoSize.Width, videoSize.Height);
            mediaRecorder.SetVideoEncoder(VideoEncoder.H263);
            mediaRecorder.SetAudioEncoder(AudioEncoder.Aac);
            //int rotation = Context.WindowManager.DefaultDisplay.Rotation;
            var windowManager = Context.GetSystemService(Context.WindowService).JavaCast<IWindowManager>();
            var rotation = (int)windowManager.DefaultDisplay.Rotation;
            int orientation = ORIENTATIONS.Get(rotation);
            mediaRecorder.SetOrientationHint(orientation);
            mediaRecorder.Prepare();
        }

        private Java.IO.File GetVideoFile()
        {
            _fileName = "video-" + DateTime.Now.ToString("yymmdd-hhmmss") + ".mp4";
             var file = new Java.IO.File(Android.OS.Environment.ExternalStorageDirectory, _fileName);
             return file;

        }

        //public Android.Graphics.Bitmap StringToBitMap(string encodedString)
        //{
        //    try
        //    {
        //        byte[] encodeByte = Android.Util.Base64.Decode(encodedString, Android.Util.Base64.Default);
        //        Android.Graphics.Bitmap bitmap = BitmapFactory.DecodeByteArray(encodeByte, 0, encodeByte.Length);
        //        return bitmap;
        //    }
        //    catch (System.Exception e)
        //    {
        //        //e.getMessage();
        //        return null;
        //    }
        //}

        public void ConfigureTransform(int viewWidth, int viewHeight)
        {
            if (Context == null || previewSize == null || Control == null)
                return;

            var windowManager = Context.GetSystemService(Context.WindowService).JavaCast<IWindowManager>();
            var rotation = (int)windowManager.DefaultDisplay.Rotation;
            var matrix = new Matrix();
            var viewRect = new RectF(0, 0, viewWidth, viewHeight);
            var bufferRect = new RectF(0, 0, previewSize.Height, previewSize.Width);
            float centerX = viewRect.CenterX();
            float centerY = viewRect.CenterY();
            if ((int)SurfaceOrientation.Rotation90 == rotation || (int)SurfaceOrientation.Rotation270 == rotation)
            {
                bufferRect.Offset((centerX - bufferRect.CenterX()), (centerY - bufferRect.CenterY()));
                matrix.SetRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.Fill);
                float scale = System.Math.Max(
                    (float)viewHeight / previewSize.Height,
                    (float)viewHeight / previewSize.Width);
                matrix.PostScale(scale, scale, centerX, centerY);
                matrix.PostRotate(90 * (rotation - 2), centerX, centerY);
            }
            Control.SetTransform(matrix);
            
            //_watermark = new WatermarkSurface(Context);
        }

        public void UpdatePreview()
        {
            if (null == cameraDevice)
                return;

            try
            {
                SetUpCaptureRequestBuilder(previewBuilder);
                HandlerThread thread = new HandlerThread("CameraPreview");
                thread.Start();
                previewSession.SetRepeatingRequest(previewBuilder.Build(), null, backgroundHandler);
            }
            catch (CameraAccessException e)
            {
                e.PrintStackTrace();
            }
        }

        private Android.Util.Size ChooseVideoSize(Android.Util.Size[] choices)
        {
            foreach (Android.Util.Size size in choices)
            {
                if (size.Width == size.Height * 4 / 3 && size.Width <= 1000)
                    return size;
            }
            //Log.Error(TAG, "Couldn't find any suitable video size");
            return choices[choices.Length - 1];
        }

        private Android.Util.Size ChooseOptimalSize(Android.Util.Size[] choices, int width, int height, Android.Util.Size aspectRatio)
        {
            var bigEnough = new List<Android.Util.Size>();
            int w = aspectRatio.Width;
            int h = aspectRatio.Height;
            foreach (Android.Util.Size option in choices)
            {
                if (option.Height == option.Width * h / w &&
                    option.Width >= width && option.Height >= height)
                    bigEnough.Add(option);
            }

            if (bigEnough.Count > 0)
                return (Android.Util.Size)Collections.Min(bigEnough, new CompareSizesByArea());
            else
            {
                //Log.Error(TAG, "Couldn't find any suitable preview size");
                return choices[0];
            }
        }

        private void SetUpCaptureRequestBuilder(CaptureRequest.Builder builder)
        {
            builder.Set(CaptureRequest.ControlMode, new Java.Lang.Integer((int)ControlMode.Auto));

        }

        private void StartBackgroundThread()
        {
            backgroundThread = new HandlerThread("CameraBackground");
            backgroundThread.Start();
            backgroundHandler = new Handler(backgroundThread.Looper);
        }

        private void StopBackgroundThread()
        {
            backgroundThread.QuitSafely();
            try
            {
                backgroundThread.Join();
                backgroundThread = null;
                backgroundHandler = null;
            }
            catch (InterruptedException e)
            {
                e.PrintStackTrace();
            }
        }

        public void OnClick()
        {
            if (isRecordingVideo)
            {
                ((CameraView)Element).IsRecording = false;
                StopRecordingVideo();
            }
            else
            {
                ((CameraView)Element).IsRecording = true;
                StartRecordingVideo();
            }
        }

        private void StartRecordingVideo()
        {
            try
            {
                //UI
                // buttonVideo.SetText(Resource.String.stop);
                isRecordingVideo = true;
                StartBackgroundThread();
                //Start recording
                mediaRecorder.Start();
            }
            catch (IllegalStateException e)
            {
                e.PrintStackTrace();
            }
        }

        public void StopRecordingVideo()
        {
            //UI
            isRecordingVideo = false;
            // buttonVideo.SetText(Resource.String.record);

            if (Context != null)
            {
                Toast.MakeText(Context, "Video saved ",
                    ToastLength.Short).Show();
                Element.CompleteCommand?.Execute(_fileName);
            }
            //Stop recording
            /*
			mediaRecorder.Stop ();
			mediaRecorder.Reset ();
			startPreview ();
			*/

            // Workaround for https://github.com/googlesamples/android-Camera2Video/issues/2
            CloseCamera();
            //OpenCamera(Control.Width, Control.Height);
            StopBackgroundThread();
        }

        private void CloseCamera()
        {
            try
            {
                cameraOpenCloseLock.Acquire();
                if (cameraDevice != null)
                {
                    cameraDevice.Close();
                    cameraDevice = null;
                }
                if (mediaRecorder != null)
                {
                    mediaRecorder.Release();
                    mediaRecorder = null;
                }
            }
            catch (InterruptedException e)
            {
                throw new RuntimeException("Interrupted while trying to lock camera closing.");
            }
            finally
            {
                cameraOpenCloseLock.Release();
            }
        }

        protected override void OnElementChanged(ElementChangedEventArgs<CameraView> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                e.NewElement.RequestStartRecord += OnClick;
                e.NewElement.RequestStopRecord += OnClick;
                var textureView = new AutoFitTextureView(Context);
                var textView = new TextView(Context);
                //textView.Text="Sample Text";
                /// textureView.add
                //textView.color
                SetNativeControl(textureView);
            }
            else
            {
                e.NewElement.RequestStartRecord -= OnClick;
                e.NewElement.RequestStopRecord -= OnClick;
            }
            if (Control != null)
            {
                if (!Control.IsAvailable)
                    Control.SurfaceTextureListener = surfaceTextureListener;

            }
        }

        private class CompareSizesByArea : Java.Lang.Object, Java.Util.IComparator
        {
            public int Compare(Java.Lang.Object lhs, Java.Lang.Object rhs)
            {
                // We cast here to ensure the multiplications won't overflow
                if (lhs is Android.Util.Size && rhs is Android.Util.Size)
                {
                    var right = (Android.Util.Size)rhs;
                    var left = (Android.Util.Size)lhs;
                    return Long.Signum((long)left.Width * left.Height -
                        (long)right.Width * right.Height);
                }
                else
                    return 0;

            }
        }

        //public static Android.Graphics.Bitmap Mark(Android.Graphics.Bitmap src, string watermark, Android.Graphics.Point location, Android.Graphics.Color color, int alpha, int size, bool underline)
        //{
        //    int w = src.Width;
        //    int h = src.Height;
        //    Android.Graphics.Bitmap result = Android.Graphics.Bitmap.CreateBitmap(w, h, src.GetConfig());

        //    Canvas canvas = new Canvas(result);
        //    canvas.DrawBitmap(src, 0, 0, null);

        //    Paint paint = new Paint();
        //    paint.Color = color; ;
        //    paint.Alpha=alpha;
        //    paint.TextSize=size;
        //    paint.AntiAlias=true;
        //    paint.UnderlineText=underline;
        //    canvas.DrawText(watermark, location.X, location.Y, paint);

        //    return result;
        //}

    }


}