﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Hardware.Camera2;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace VideoRecApp.Droid.Renderers
{
    public class PreviewCaptureStateCallback : CameraCaptureSession.StateCallback
    {
        CameraViewRenderer _cameraViewRenderer;
        public PreviewCaptureStateCallback(CameraViewRenderer cameraViewRenderer)
        {
            _cameraViewRenderer = cameraViewRenderer;
        }
        public override void OnConfigured(CameraCaptureSession session)
        {
            _cameraViewRenderer.previewSession = session;
            _cameraViewRenderer.UpdatePreview();

        }

        public override void OnConfigureFailed(CameraCaptureSession session)
        {
            if (null != _cameraViewRenderer.Context)
                Toast.MakeText(_cameraViewRenderer.Context, "Failed", ToastLength.Short).Show();
        }
    }
}