﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Hardware.Camera2;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace VideoRecApp.Droid.Renderers
{
    public class MyCameraStateCallback : CameraDevice.StateCallback
    {
        CameraViewRenderer _cameraViewRenderer;
        public MyCameraStateCallback(CameraViewRenderer cameraViewRenderer)  
        {
            _cameraViewRenderer = cameraViewRenderer;
        }
        public override void OnOpened(CameraDevice camera)
        {
            _cameraViewRenderer.cameraDevice = camera;
            _cameraViewRenderer.StartPreview();
            _cameraViewRenderer.cameraOpenCloseLock.Release();
            if (null != _cameraViewRenderer.Control)
                _cameraViewRenderer.ConfigureTransform(_cameraViewRenderer.Control.Width, _cameraViewRenderer.Control.Height);
        }

        public override void OnDisconnected(CameraDevice camera)
        {
            _cameraViewRenderer.cameraOpenCloseLock.Release();
            camera.Close();
            _cameraViewRenderer.cameraDevice = null;
        }

        public override void OnError(CameraDevice camera, CameraError error)
        {
            _cameraViewRenderer.cameraOpenCloseLock.Release();
            camera.Close();
            _cameraViewRenderer.cameraDevice = null;
            if (null != _cameraViewRenderer.Context) { }
               // _cameraViewRenderer.Context.Finish();
        }


    }
}