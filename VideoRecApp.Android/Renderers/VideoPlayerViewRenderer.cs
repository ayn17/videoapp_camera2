﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;
using VideoRecApp.CustomRenderer;
using VideoRecApp.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(VideoPlayerView), typeof(VideoPlayerViewRenderer))]
namespace VideoRecApp.Droid.Renderers
{
    public class VideoPlayerViewRenderer : ViewRenderer<VideoPlayerView, VideoView>, MediaPlayer.IOnPreparedListener
    {
        private bool isPrepared;
        MediaController mediaController;

        public VideoPlayerViewRenderer(Context context) : base(context)
        {
            mediaController = new MediaController(Context);
        }

        public void OnPrepared(MediaPlayer mp)
        {
            isPrepared = true;
        }

        protected override void OnElementChanged(ElementChangedEventArgs<VideoPlayerView> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                SetNativeControl(new VideoView(Context));
                if (Control != null)
                {
                    Control.RequestFocus();
                    Control.SetOnPreparedListener(this);
                }

                e.NewElement.PlayRequest += PlayVideo;
                e.NewElement.StopRequest += StopVideo;
                e.NewElement.PauseRequest += PauseVideo;
            }
            else
            {
                e.NewElement.PlayRequest -= PlayVideo;
                e.NewElement.StopRequest -= StopVideo;
                e.NewElement.PauseRequest -= PauseVideo;
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == VideoPlayerView.PathProperty.PropertyName)
            {
                if (!string.IsNullOrEmpty(Element.Path))
                {
                    var directory = Path.Combine(Android.OS.Environment.ExternalStorageDirectory.AbsolutePath, Element.Path);
                    Control.SetVideoPath(directory);
                    Control.SetMediaController(mediaController);
                    mediaController.SetAnchorView(Control);
                    Control.Start();



                }

            }

        }

        private void PlayVideo()
        {
            if (isPrepared && !string.IsNullOrEmpty(Element.Path))
            {
                Control.Start();
            }
        }

        private void StopVideo()
        {
            if (Control.IsPlaying)
            {

            }
        }

        private void PauseVideo()
        {
            if (Control.IsPlaying)
            {
                Control.Pause();
            }
        }
    }
}